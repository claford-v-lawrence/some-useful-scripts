SetCapsLockState, AlwaysOff

#SingleInstance ignore
Hotkey, I, Off
Hotkey, J, Off
Hotkey, K, Off
Hotkey, L, Off

^t::
    Hotkey, I, Toggle
    Hotkey, J, Toggle
    Hotkey, K, Toggle
    Hotkey, L, Toggle
return

I::
    Send {Up}
return

J::
    Send {Left}
return

K::
    Send {Down}    
return

L::
    Send {Right} 
return