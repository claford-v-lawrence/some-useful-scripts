#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#InstallKeybdHook
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


; remap the f4
F4::Browser_Home
Browser_Home::F4

;for some reason, the f5 seems to be a macro.
;therefore, i didn't map it
; remap f6
F6::AppsKey
AppsKey::F6

; remap f7
Browser_Back::F7
F7::Browser_Back

F8::Media_Prev
Media_Prev::F8
; remap f9
F9::Media_Play_Pause
Media_Play_Pause::F9
; try
F10::Media_Next
Media_Next::F10
;
F11::Volume_Mute
Volume_Mute::F11

F12::Volume_Down
Volume_Down::F12

Insert::Volume_Up
Volume_Up::Insert

; the old setup that I decided to change. Apparently using the side of my palm
; is a far better idea for typing
; remap the caps lock key to be a control key as now I cannot get many of thos
;`;::LControl
;~Shift & `;::Send, {:}
;~CapsLock & `;::Send, {;}
;~CapsLock & Enter::Send, ^{Enter}

;a & Enter::Send, ^{Enter}
; SC03A & h::Send, {Left}
; SC03A & j::Send, {Down}
; SC03A & k::Send, {Up}
; SC03A & l::Send, {Right}
; CapsLock::
;     if GetKeyState("CapsLock", "T") = 1
;     {
;         SetCapsLockState, on
;     }
;     else if GetKeyState("CapsLock", "F") = 0
;     {
;         SetCapsLockState, off
;     }
;     Return
; ; now pressing FNN


;;confirm: this actually works         
~Capslock & i::Send,{Up}
~CapsLock & j::Send,{Left}
~CapsLock & k::Send,{Down}
~CapsLock & l::Send,{Right}

#If GetKeyState("Capslock", "T")=1
j::Send,{Left}
l::Send,{Right}
i::Send,{Up}
k::Send,{Down}
u::Send,{PgUp}
o::Send,{PgDn}
#If ; end of #If